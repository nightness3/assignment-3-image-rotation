#include "../../include/transition.h"

struct image rotate( struct image const source ) {
	struct image rotated_img = createImage( source.height, source.width );
	for (uint64_t i = 0; i < rotated_img.height; i++) {
		for (uint64_t j = 0; j < rotated_img.width; j++) {
			rotated_img.data[i*rotated_img.width + j] = source.data[(source.height - 1 - j)*source.width + i];
		}
	}
	return rotated_img;
}

