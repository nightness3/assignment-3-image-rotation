#include "../include/bmp_outer_image_format.h"
#include "../include/transition.h"

int main( int argc, char** argv ) {
	//(void) argc; (void) argv; // supress 'unused parameters' warning

	if ( argc != 3 ) {
		printf("Invalid number of arguments");
		return 1;
	}

	// Open file

	FILE** in_file = malloc( sizeof( FILE* ) );
	FILE** out_file = malloc( sizeof( FILE* ) );

	enum open_status in_file_open_status = open_file( in_file, argv[1], "r" );
	enum open_status out_file_open_status = open_file( out_file, argv[2], "w");

	if ( in_file_open_status == OPEN_OK ) {
		printf("Success open in file\n");
	}
	else if ( in_file_open_status == OPEN_ERROR ) {
		printf("Error when opening in file\n");
		return 1;
	}

	if ( out_file_open_status == OPEN_OK ) {
		printf("Success open out file\n");
	}
	else if ( out_file_open_status == OPEN_ERROR ) {
		printf("Error when opening out file\n");
		return 1;
	}

	// Read from BMP file to image struct

	struct image *img = malloc( sizeof( struct image ) );

	enum read_status in_file_read_status = from_bmp( *in_file, img );

	if ( in_file_read_status == READ_OK ) {
		printf("Success reading in file\n");
	}
	else if ( in_file_read_status == READ_INVALID_SIGNATURE ) {
		printf("Error: BMP file invalid signature\n");
		free(img);
		return 1;
	}
	else if ( in_file_read_status == READ_INVALID_BITS ) {
		printf("Error: BMP file invalid bits\n");
		free(img);
		return 1;
	}
	else if ( in_file_read_status == READ_INVALID_HEADER ) {
		printf("Error: BMP file invalid header\n");
		free(img);
		return 1;
	}
	// Rotate image struct

	struct image* rotated_img = malloc( sizeof( struct image ) );

	*rotated_img = rotate( *img );

	printf("Success rotated image\n");

	// Write from image struct to BMP file

	enum write_status out_file_write_status = to_bmp( *out_file, rotated_img );

	if ( out_file_write_status == WRITE_OK ) {
		printf("Success write out file\n");
	}
	else if ( out_file_write_status == WRITE_OK ) {
		destroyImage(rotated_img);
		free(rotated_img);
		free(img);
		printf("Error when writing file");
		return 1;
	}

	// Close file

	enum close_status in_file_close_status = close_file( *in_file );
	enum close_status out_file_close_status = close_file( *out_file );

	if ( in_file_close_status == CLOSE_OK ) {
		printf("Success close in file\n");
	}
	else if ( in_file_close_status == CLOSE_ERROR ) {
		destroyImage(rotated_img);
		free(rotated_img);
		free(img);
		printf("Error when closing in file\n");
		return 1;
	}

	if ( out_file_close_status == CLOSE_OK ) {
		printf("Success close out file\n");
	}
	else if ( out_file_close_status == CLOSE_ERROR ) {
		destroyImage(rotated_img);
		free(rotated_img);
		free(img);
		printf("Error when closing out file\n");
		return 1;
	}

	free(in_file);
	free(out_file);
	destroyImage(img);
	destroyImage(rotated_img);
	free(img);
	free(rotated_img);

	return 0;
}
