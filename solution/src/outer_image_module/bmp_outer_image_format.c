#include "../../include/bmp_outer_image_format.h"

const uint16_t VALID_BMP_TYPE_1 = 0x4d42;
const uint16_t VALID_BMP_TYPE_2 = 0x4349;
const uint16_t VALID_BMP_TYPE_3 = 0x5450;

const uint32_t BMP_OFF_BITS = 54;
const uint32_t BMP_IMAGE_SIZE = 40;

const uint16_t BMP_IMAGE_PLANES = 3;
const uint16_t BMP_IMAGE_BITCOUNT = 24;
const uint32_t BMP_IMAGE_COMPRESSION = 0;

int64_t img_padding ( uint64_t const width ) {
	return ((((int64_t) width) * 3 + 3) & (-4)) - (((int64_t) width) * 3);
}

enum open_status open_file ( FILE** const file, char* const path, char* const mode) {
	*file = fopen( path, mode );
	if ( *file == NULL ) {
		return OPEN_ERROR;
	} else {
		return OPEN_OK;
	}
}

enum close_status close_file ( FILE* const file ) {
	int status = fclose ( file );
	if ( status == EOF ) {
		return CLOSE_ERROR;
	} else {
		return CLOSE_OK;
	}
}

enum read_status from_bmp( FILE* const in, struct image* img ) {
	struct bmp_header file_header;

	if ( !fread( &file_header, sizeof(struct bmp_header), 1, in ) ) {
		return READ_INVALID_HEADER;
	}

	if ( file_header.bfType != VALID_BMP_TYPE_1 && file_header.bfType != VALID_BMP_TYPE_2 && file_header.bfType != VALID_BMP_TYPE_3 ) {
		return READ_INVALID_SIGNATURE;
	}

	if ( file_header.biBitCount != 24 ) {
		return READ_INVALID_BITS;
	}

	*img = createImage( file_header.biWidth, file_header.biHeight );

	int64_t img_byte_padding = img_padding(img->width);

	for ( uint64_t i = 0; i < img->height; i++ ) {
		if ( fread( img->data + img->width * i, sizeof(struct pixel), img->width, in) != img->width ) {
			return READ_ERROR;
		}
		if ( fseek( in, img_byte_padding, SEEK_CUR ) ) {
			return READ_ERROR;
		}
	}

	return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img) {
	int64_t img_byte_padding = img_padding(img->width);
	struct bmp_header file_header = (struct bmp_header) {
		.bfType = VALID_BMP_TYPE_1,
		.bOffBits = BMP_OFF_BITS,
		.biSize = BMP_IMAGE_SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = BMP_IMAGE_PLANES,
		.biBitCount = BMP_IMAGE_BITCOUNT,
		.biCompression = BMP_IMAGE_COMPRESSION,
		.biSizeImage = (img->width * sizeof(struct pixel) + img_byte_padding) * img->height,
		.bfileSize = (img->width * sizeof(struct pixel) + img_byte_padding) * img->height + sizeof(struct bmp_header)
	};

	if ( !fwrite( &file_header, sizeof(struct bmp_header), 1, out ) ) {
		return WRITE_ERROR;
	}

	struct pixel *garbage_pixel = img->data;

	for (uint64_t i = 0; i < img->height; i++) {
		if ( !fwrite( img->data + i*img->width, sizeof(struct pixel) * img->width, 1, out ) ) {
			return WRITE_ERROR;
		}
		if ( !fwrite(  garbage_pixel, img_byte_padding, 1, out ) && img_byte_padding != 0 ) {
			return WRITE_ERROR;
		}
	}

	return WRITE_OK;
}
