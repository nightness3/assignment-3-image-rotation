#include "../../include/inner_image_format.h"
#include <stddef.h>

struct image createImage (uint64_t width, uint64_t height) {
	return (struct image) { .width = width, .height = height, .data = malloc(sizeof(struct pixel)*width*height)};
}

void destroyImage (struct image* imageForDestroy) {
	free(imageForDestroy->data);
}
