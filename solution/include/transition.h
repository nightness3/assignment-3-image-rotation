#ifndef TRANSITION_HEADER
#define TRANSITION_HEADER

#include "inner_image_format.h"

struct image rotate( struct image const source );

#endif
