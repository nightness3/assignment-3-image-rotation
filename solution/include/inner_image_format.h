#ifndef INNER_IMAGE_FORMAT_HEADER
#define INNER_IMAGE_FORMAT_HEADER

#include <stdint.h>
#include <stdlib.h>

struct image {
	uint64_t width, height;
	struct pixel* data;
};

struct __attribute__((packed)) pixel {
	uint8_t b, g, r;
};

struct image createImage (uint64_t width, uint64_t height);

void destroyImage (struct image* imageForDestroy);

#endif
