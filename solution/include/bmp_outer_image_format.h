#ifndef BMP_OUTER_IMAGE_FORMAT_HEADER
#define BMP_OUTER_IMAGE_FORMAT_HEADER

#include "bmp_header_outer_image_format.h"
#include "inner_image_format.h"

enum read_status  {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_ERROR
};

enum read_status from_bmp( FILE* const in, struct image* img );

enum  write_status  {
	WRITE_OK = 0,
	WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

enum open_status {
	OPEN_OK = 0,
	OPEN_ERROR
};

enum open_status open_file( FILE** const file, char* const path, char* const mode );

enum close_status {
	CLOSE_OK = 0, 
	CLOSE_ERROR
};

enum close_status close_file( FILE* const file );

#endif
